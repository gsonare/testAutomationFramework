package gourav.pageobjects.homepage;

import gourav.framework.core.testInitialization.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleHomePage extends BasePage {
    public GoogleHomePage(WebDriver driver) {
        super(driver);
    }

    public void Google(){
//        driver.get("https://www.google.com");
        String A = driver.getTitle();
        String E = "google";
        if(E.equalsIgnoreCase(A)){
            System.out.println("Executing as per the expectation");
        }
        else {
            System.out.println("Not executing as per the expectation");
        }
//        WebElement element = driver.findElement(By.xpath("//span[@class='hb2Smf']"));
//        element.click();

        waitForElementToAppear(By.linkText("Gmail"));
    }
}