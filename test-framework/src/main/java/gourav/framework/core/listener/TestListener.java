package gourav.framework.core.listener;

import gourav.framework.core.testInitialization.BrowserInitializer;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;

public class TestListener implements ITestListener {

    public void onTestStart(ITestResult var1) {
        System.out.println("This Test Step has been started");
    }

    public void onTestSuccess(ITestResult var1) {
        System.out.println("Dahi ho gaya");
    }

    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("*****Error " + iTestResult.getName() + " test has failed*****");
        Object testClass = iTestResult.getInstance();
        System.out.println(testClass);
        WebDriver driver = ((BrowserInitializer)testClass).getDriver();
        if (driver instanceof WebDriver) {
            try {
                saveScreenshotPNG(driver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    @Attachment(value = "Page screenshot", type = "image/png")
    public void saveScreenshotPNG(WebDriver driver) throws IOException {
        System.out.println("This is capturing Screenshot");
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        System.out.println(srcFile);
        File targetFile = new File(".\\resources\\"+driver.getClass()+".png");
//        FileUtils.copyFile(srcFile, targetFile);
//        return
    }

    public void onTestSkipped(ITestResult var1) {
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult var1) {
    }

    public void onStart(ITestContext var1) {
    }

    public void onFinish(ITestContext var1) {
    }
}