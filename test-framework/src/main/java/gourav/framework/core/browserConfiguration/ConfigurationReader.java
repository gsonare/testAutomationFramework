package gourav.framework.core.browserConfiguration;

import com.esotericsoftware.yamlbeans.YamlReader;
import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ConfigurationReader {

    public static Map<String, Configuration> configMap = new HashMap<>();

    private static final Logger logger = Logger.getLogger(ConfigurationReader.class);

    /**
     * This method reads the config.yaml file and creates config object
     *
     * @return - This will return
     **/
    public static Map<String, Configuration> yamlConfigurationReader() {

        try {
//            YamlReader reader = new YamlReader(new FileReader("C:\\Users\\gsonare\\gouravSonare\\Test\\Automation\\testAutomationFramework\\config.yaml"));
            YamlReader reader = new YamlReader(new FileReader("C:\\Users\\gsonare\\gouravSonare\\Test\\Automation\\SNCF_webAutomation\\config.yaml"));
            HashMap<String, ArrayList<String>> map = (HashMap<String, ArrayList<String>>) reader.read();

            for (Map.Entry<String, ArrayList<String>> hm : map.entrySet()) {
                HashMap<String, String> hmConfiguration = new HashMap<>();
                Configuration configuration = new Configuration();

                ArrayList<String> arrListValue = hm.getValue();
                for (String key : arrListValue) {
                    String[] strArray = key.split(":");
                    hmConfiguration.put(strArray[0], strArray[1]);
                }
                configuration.setBrowserName(hmConfiguration.get("browserName"));
                configuration.setPlatformName(hmConfiguration.get("platformName"));
                configuration.setBrowserVersion(hmConfiguration.get("browserVersion"));
                configuration.setSslCertificate(Boolean.parseBoolean(hmConfiguration.get("sslCertificate")));
                configuration.setHeadless(Boolean.parseBoolean(hmConfiguration.get("headless")));
                configuration.setAppURL(hmConfiguration.get("appURL"));
                configMap.put(hm.getKey(), configuration);

                System.out.println("Platform Name is:" + " " +configuration.getPlatformName());
               }
            }
            catch(IOException ex){
                logger.error(ex);
//                ex.printStackTrace();
            }
        return configMap;
    }
}