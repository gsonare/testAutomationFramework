package gourav.framework.core.browserConfiguration;

public class Configuration {

    private String browserName;
    private String platformName;
    private String browserVersion;
    private boolean sslCertificate;
    private boolean headless;
    private String appURL;

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public boolean getSslCertificate() {
        return sslCertificate;
    }

    public void setSslCertificate(boolean sslCertificate) {
        this.sslCertificate = sslCertificate;
    }

    public boolean getHeadless(){
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public String getAppURL(){
        return appURL;
    }

    public void setAppURL(String appURL){
        this.appURL = appURL;
    }
}