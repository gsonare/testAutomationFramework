package gourav.framework.core.utility;

public enum Actions {
    CLICK,
    COMPARE,
    COMPARE_PART,
    FIND,
    SET,
    GET,
    ELEMENT_PRESENT,
    ELEMENT_ABSENT,
    TEXT_ABSENT,
    TEXT_CHANGED,
    SWITCH_ENABLED,
    SWITCH_DISABLED
}