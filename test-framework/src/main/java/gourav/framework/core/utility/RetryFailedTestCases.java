package gourav.framework.core.utility;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryFailedTestCases implements IRetryAnalyzer{

	private int retryCount = 0;
	
	private int maxRetryCount = 2;
	
	@Override
	public boolean retry(ITestResult result) {
		// TODO Auto-generated method stub
		if(retryCount < maxRetryCount)
		{
			System.out.println("Retrying " +result.getName() + " again and the count is " + (retryCount+1));
			retryCount++;
			return true;
		}
		return false;
	}
}