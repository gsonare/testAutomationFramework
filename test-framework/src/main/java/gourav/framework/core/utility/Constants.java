package gourav.framework.core.utility;

public class Constants {

    /**  *********************** Browser Drivers ***************************************** */
    public static String chrome = "C:\\Users\\gsonare\\Documents\\Applications and Drivers\\chromedriver.exe";
    public static String gecko = "C:\\Users\\gsonare\\Documents\\Applications and Drivers\\geckodriver.exe";
    public static String ie = "C:\\Users\\gsonare\\Documents\\Applications and Drivers\\IEDriverServer.exe";
    /** *********** Result Constants *********/
    public static final String PASS = "Pass";
    public static final String FAIL = "Fail";
}
