package gourav.framework.core.utility;

public enum Locators {
    ID, XPATH, CLASS_NAME, CSS, LINK_TEXT, PARTIAL_LINK_TEXT, NAME
}