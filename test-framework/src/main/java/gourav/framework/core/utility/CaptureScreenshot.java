package gourav.framework.core.utility;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class CaptureScreenshot {
	
	public static void captureFailedScreenshot(WebDriver driver, String ScreenshotName)
	{
		try
		{
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			File destination = new File("./Screenshots/Failed/"+ScreenshotName+".png");
			FileUtils.copyFile(source, destination);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("Error while taking screenshot for failed testcase "+e.getMessage());
		}
		
		System.out.println("Screenshot for failed testcase has taken");
	}
	
	public static void capturePassedScreenshot(WebDriver driver, String ScreenshotName)
	{
		try
		{
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			File destination = new File("./Screenshots/Passed/"+ScreenshotName+".png");
			FileUtils.copyFile(source, destination);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("Error while taking screenshot for passed testcase "+e.getMessage());
		}
		
		System.out.println("Screenshot for passed testcase has taken");
	}
	
	public static void captureSkippedScreenshot(WebDriver driver, String ScreenshotName)
	{
		try
		{
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			File destination = new File("./Screenshots/Skipped/"+ScreenshotName+".png");
			FileUtils.copyFile(source, destination);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("Error while taking screenshot for skipped testcase "+e.getMessage());
		}
		
		System.out.println("Screenshot for skipped testcase has taken");
	}
	
	public static void captureScreenshot(WebDriver driver, String ScreenshotName)
	{
		try
		{
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			File destination = new File("./Screenshots/"+ScreenshotName+".png");
			FileUtils.copyFile(source, destination);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("Error while taking screenshot "+e.getMessage());
		}
		
		System.out.println("Screenshot taken");	
	}
}