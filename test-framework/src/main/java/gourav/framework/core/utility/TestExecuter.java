package gourav.framework.core.utility;

import gourav.framework.core.browserConfiguration.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TestExecuter {

    private WebDriver driver;
    FileWriter fw;
    String filePath;

    public TestExecuter(String device) throws Exception {

        try {
            filePath = Utils.getFilePath();
            String fileName = filePath + "test_result.txt";
            File file = new File(fileName);
            file.getParentFile().mkdirs();
            fw = new FileWriter(file);
        } catch (IOException ex) {
//            logger.error(ex);
            ex.printStackTrace();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void writeResults(String str) {
//        logger.info(str);
        try {
            fw.write(str + System.lineSeparator());//appends the string to the file
            fw.flush();
        } catch (IOException ex) {
//            logger.error(ex);
            ex.printStackTrace();
            try {
                fw.close();
            } catch (IOException e) {
//                logger.error(e);
                e.printStackTrace();
            }
        }
    }

    public WebElement waitUntil(Locators locator, String access, int timeout) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            if (locator == Locators.ID) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(access)));
            } else if (locator == Locators.XPATH) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(access)));
            } else if (locator == Locators.CSS) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(access)));
            } else if (locator == Locators.CLASS_NAME) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(access)));
            } else if (locator == Locators.LINK_TEXT) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(access)));
            } else if (locator == Locators.PARTIAL_LINK_TEXT) {
                //element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(access)));
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(access)));
            }
        } catch (Exception ex) {
//            logger.error("Element not found " + ex);
            ex.printStackTrace();
        }
        return element;
    }

    private WebElement getElement(Locators locator, String access, int timeout) {
        WebElement element = null;
        if (locator == Locators.ID) {
            element = waitUntil(Locators.ID, access, timeout);
        } else if (locator == Locators.XPATH) {
            element = waitUntil(Locators.XPATH, access, timeout);
        } else if (locator == Locators.CSS) {
            element = waitUntil(Locators.CSS, access, timeout);
        } else if (locator == Locators.CLASS_NAME) {
            element = waitUntil(Locators.CLASS_NAME, access, timeout);
        } else if (locator == Locators.LINK_TEXT) {
            element = waitUntil(Locators.LINK_TEXT, access, timeout);
        } else if (locator == Locators.NAME){
            element = waitUntil(Locators.NAME, access, timeout);
        } else if (locator == Locators.PARTIAL_LINK_TEXT){
            element = waitUntil(Locators.PARTIAL_LINK_TEXT, access, timeout);
        }
        return element;
    }

    private List<WebElement> getElements(Locators locator, String access) {
        List<WebElement> elements = null;
        elements = driver.findElements(By.className("XCUIElementTypeStaticText"));
        return elements;
    }

    public String performAction(Actions action, Locators locator, String access, String message) {
        return performAction(action, locator, access, message, null);
    }

    /**
     * This is the helper method which is used to call various other methods to perform various actions on elements
     *
     * @param action
     * @param locator
     * @param access
     * @param message
     * @param send
     * @return
     */
    public String performAction(Actions action, Locators locator, String access, String message, String send) {
        String testStatus = "";
        try {
            switch (action) {
                case CLICK:
                    testStatus = clickAction(locator, access, message);
                    break;
                case COMPARE:
                    testStatus = compareAction(locator, access, message, send);
                    break;
                case COMPARE_PART:
                    testStatus = comparePartAction(locator, access, message, send);
                    break;
                case GET:
                    testStatus = getTextAction(locator, access, message);
                    break;
                case SET:
                    testStatus = setTextAction(locator, access, message, send);
                    break;
                case ELEMENT_PRESENT:
                    testStatus = checkElementPresent(locator, access, message);
                    break;
                case ELEMENT_ABSENT:
                    testStatus = checkElementAbsence(locator, access, message);
                    break;
                case TEXT_ABSENT:
                    testStatus = checkTextAbsence(locator, access, message, send);
                    break;
                case TEXT_CHANGED:
                    testStatus = checkTextChanged(locator, access, message, send);
                    break;
                case SWITCH_ENABLED:
                    testStatus = checkSwitchEnabled(locator, access, message);
                    break;
                case SWITCH_DISABLED:
                    testStatus = checkSwitchDisabled(locator, access, message);
                    break;
            }
        } catch (Exception ex) {
//            logger.error("ERROR: :" + ex);
            ex.printStackTrace();
            // screenshot("Step_"+currentStep+"_"+message+"_"+action.toString());
        }
        return testStatus;
    }

//    @Step("Click on {message}")
    public String clickAction(Locators method, String access, String message) throws Exception {
        WebElement element = getElement(method, access, 45);
        Assert.assertNotNull(element);
        if (element != null) {
            element.click();
            writeResults("PASS: Clicked on " + message);
            return Constants.PASS;
        } else {
            writeResults("FAIL: An error occurred while clicking on  " + message + ", see screenshots folder for more information ");
            return Constants.FAIL;
        }
    }

    private String compareAction(Locators locator, String access, String message, String send) {
        WebElement element = null;
        element = getElement(locator, access, 30);

        Assert.assertNotNull(element);
        if (element != null) {
            String normalizedElementText = Utils.getNormalisedString(element.getText());
            String normalizedTextToCompare = Utils.getNormalisedString(send);

            Assert.assertEquals(normalizedElementText.trim().toLowerCase(), normalizedTextToCompare.toLowerCase());

            if (normalizedElementText.trim().equalsIgnoreCase(normalizedTextToCompare)) {
                writeResults("PASS: Found the text- " + send + " on " + message);
                return Constants.PASS;
            } else {
                writeResults("FAIL: Not found text- " + send);
                return Constants.FAIL;
            }
        }
        return null;
    }

    /**
     * This method is used to check if the text on the element provided in {access} contains the text provided
     * in the {send} parameter
     *
     * @param locator
     * @param access
     * @param message
     * @param send
     * @return
     * @throws Exception
     */
//    @Step("Test if the element contain text: {send}")
    private String comparePartAction(Locators locator, String access, String message, String send) {
        WebElement element = null;
        element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            String normalizedElementText = Utils.getNormalisedString(element.getText());
            String normalizedTextToCompare = Utils.getNormalisedString(send);
            Assert.assertTrue(normalizedElementText.toLowerCase().contains(normalizedTextToCompare.toLowerCase()));
            if (normalizedElementText.contains(normalizedTextToCompare)) {
                writeResults("PASS: Found the text- " + send);
                return Constants.PASS;
            } else {
                writeResults("FAIL: Not found text- " + send);
                return Constants.FAIL;
            }
        }
        return null;
    }

    /**
     * This method returns the text on the element provided in {access} parameter
     *
     * @param locator
     * @param access
     * @param message
     * @return
     * @throws Exception
     */
//    @Step("Get text from the element")
    private String getTextAction(Locators locator, String access, String message) {
        WebElement element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            writeResults("PASS: Got the text- " + element.getText() + " from " + message);
            return element.getText();
        }
        return null;
    }

    /**
     * This method sets the text in the {send} parameter to the element in the {access} parameter
     *
     * @param locator
     * @param access
     * @param message
     * @param send
     * @return
     * @throws Exception
     */
    //todo:- implementation not complete
    private String setTextAction(Locators locator, String access, String message, String send) throws Exception {
        WebElement element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            //int price=Integer.parseInt(element.getText());
            element.sendKeys(send);
//            ((WebElement) element.sendKeys(send);
            writeResults("PASS: Set the value to " + send);
            return element.getText();
        }
        return null;
    }

    /**
     * This method checks if the element passed in {access} is present
     *
     * @param locator
     * @param access
     * @param message
     * @return
     */
//    @Step("Check for the presence of element")
    private String checkElementPresent(Locators locator, String access, String message) {
        WebElement element = null;
        element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            writeResults("PASS: Found " + message);
            return Constants.PASS;
        } else {
            writeResults("FAIL: Not found " + message);
            return Constants.FAIL;
        }
    }

    /**
     * This method checks if the element passed in {access} is not present
     *
     * @param locator
     * @param access
     * @param message
     * @return
     */
//    @Step("Check for the absence of element")
    private String checkElementAbsence(Locators locator, String access, String message) {
        WebElement element = null;
        element = getElement(locator, access, 30);
        Assert.assertNull(element);
        if (element == null) {
            writeResults("PASS: Not found " + message);
            return Constants.PASS;
        } else {
            writeResults("FAIL: Found " + message);
            return Constants.FAIL;
        }
    }

    /**
     * This method checks for the absence of text on the element provided in the {access} parameter
     *
     * @param locator
     * @param access
     * @param message
     * @param send
     * @return
     * @throws Exception
     */
//    @Step("Check for the presence text: '{3}' on element")
    private String checkTextAbsence(Locators locator, String access, String message, String send) throws Exception {

        WebElement element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            Assert.assertFalse(element.getText().contains(send));
            if (!element.getText().contains(send)) {
                writeResults("PASS: Text not present " + send);
                return Constants.PASS;
            } else {
                writeResults("FAIL: Text found " + send);
                return Constants.FAIL;
            }
        }
        return null;
    }

    /**
     * This method checks if the text on the element is {access} has changed.
     *
     * @param locator
     * @param access
     * @param message
     * @param send
     * @return
     * @throws Exception
     */
//    @Step("Check for the change in text on element")
    private String checkTextChanged(Locators locator, String access, String message, String send) {
        WebElement element = getElement(locator, access, 30);
        Assert.assertNotNull(element);
        if (element != null) {
            Assert.assertNotEquals(element.getText(), send);
            if (!element.getText().equals((send))) {
                writeResults("PASS: Text changed " + send);
                return Constants.PASS;
            } else {
                writeResults("FAIL: Text did not change " + send);
                return Constants.FAIL;
            }
        }
        return null;
    }

    /**
     * This method checks if the switch is enabled
     *
     * @param locator
     * @param access
     * @param message
     * @return
     * @throws Exception
     */
//    @Step("Check if the switch is enabled")
    private String checkSwitchEnabled(Locators locator, String access, String message) {
        WebElement element = getElement(locator, access, 30);
        Configuration configuration = Utils.getConfiguration();
        Assert.assertNotNull(element);
        if (element != null && configuration.getPlatformName().equalsIgnoreCase("Android")) {
            Assert.assertTrue(Boolean.parseBoolean(element.getAttribute("checked")));
            if (Boolean.parseBoolean(element.getAttribute("checked"))) {
                writeResults("PASS: " + message + " switch enabled ");
                return Constants.PASS;
            } else {
                writeResults("FAIL: " + message + " switch not enabled ");
                return Constants.FAIL;
            }
        } else if (element != null && configuration.getPlatformName().equalsIgnoreCase("iOS")) {

            int switchValue = Integer.parseInt(element.getAttribute("value"));
            Assert.assertTrue(switchValue == 1);
            if (switchValue == 1) {
                writeResults("PASS: " + message + " switch  enabled ");
                return Constants.PASS;
            } else {
                writeResults("FAIL: " + message + " switch not enabled ");
                return Constants.FAIL;
            }
        }
        return null;
    }

    /**
     * This method checks if the switch is disabled
     *
     * @param locator
     * @param access
     * @param message
     * @return
     * @throws Exception
     */
//    @Step("Check if the switch is disabled")
    private String checkSwitchDisabled(Locators locator, String access, String message) {
        WebElement element = getElement(locator, access, 30);
        Configuration configuration = Utils.getConfiguration();
        Assert.assertNotNull(element);
        if (element != null && configuration.getPlatformName().equalsIgnoreCase("Android")) {
            Assert.assertFalse(Boolean.parseBoolean(element.getAttribute("checked")));
            if (!Boolean.parseBoolean(element.getAttribute("checked"))) {
                writeResults("PASS: " + message + " switch not enabled ");
                return Constants.PASS;
            } else {
                writeResults("FAIL: " + message + " switch enabled ");
                return Constants.FAIL;
            }
        } else if (element != null && configuration.getPlatformName().equalsIgnoreCase("iOS")) {
            Assert.assertNotNull(element);
            int switchValue = Integer.parseInt(element.getAttribute("value"));
            if (element != null) {
                Assert.assertTrue(switchValue != 1);
                if (switchValue != 1) {
                    writeResults("PASS: " + message + " switch not enabled");
                    return Constants.PASS;
                } else {
                    writeResults("FAIL: " + message + " switch enabled ");
                    return Constants.FAIL;
                }
            }
        }
        return null;
    }
}