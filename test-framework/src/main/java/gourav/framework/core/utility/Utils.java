package gourav.framework.core.utility;

import gourav.framework.core.browserConfiguration.Configuration;
import gourav.framework.core.browserConfiguration.ConfigurationReader;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    private static final Logger logger = Logger.getLogger(Utils.class);


    public static Utils INSTANCE = new Utils();

    public static Utils getInstance()
    {
        return INSTANCE;
    }

    public static Configuration configuration;

    public static Map<String, Configuration> yamlMap;

    public static Map<String, Configuration> readYAML() {
        yamlMap = ConfigurationReader.yamlConfigurationReader();
        System.out.println("3a test>smartlink>utils>Util>readYmMl");
        return yamlMap;
    }

    /**
     * This method return Config object
     *
     * @return
     */
    public static Configuration readProperties(String device) {
        configuration = new Configuration();
        configuration = yamlMap.get(device);
        return configuration;
    }

    /**
     * This method returns a Map wih key-value pair from the objectIdsAndroid or
     * objectIdsiOS properties file
     *
     * @return
     * @throws Exception
     */
    public static Map<String, String> getObjectIds() throws Exception {
        Map<String, String> objectIdMap = new HashMap<>();
        //Config config=readProperties();

        InputStream is = null;
        Properties prop = null;

        String fileName = "";
        if (configuration.getPlatformName().equalsIgnoreCase("Android")) {
            fileName = "objectIdsAndroid.properties";
        } else if (configuration.getPlatformName().equalsIgnoreCase("iOS")) {
            fileName = "objectIdsiOS.properties";
        } else {
            throw new Exception("Unexpected platform name in config file");
        }
        try {
            prop = new Properties();
            is = new FileInputStream("./" + fileName);
            prop.load(is);
        } catch (FileNotFoundException e) {
            logger.error("File Not Found:"+e);
            BasicConfigurator.configure();
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(e);
            BasicConfigurator.configure();
            e.printStackTrace();
        }

        Set<Map.Entry<Object, Object>> entries = prop.entrySet();
        for (Map.Entry<Object, Object> entry : entries) {
            objectIdMap.put((String) entry.getKey(), ((String) entry.getValue()).trim());
        }
        return objectIdMap;
    }

    public static String getFilePath() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
        Date date = new Date();

        String filePath = "." + File.separator + "test_results" + File.separator + formatter.format(date) + File.separator;
        return filePath;
    }

    /**
     * This method normalises the ASCII characters to english characters.
     *
     * @param str
     * @return
     */
    public static String getNormalisedString(String str) {
        return Normalizer
                .normalize(str.trim(), Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    public static Configuration getConfiguration() {
        return configuration;
    }

    /**
     * This method creates the Environment.properties file in allure-results
     */
    public static void updateEnvironmentFile() {
        OutputStream os = null;
        Properties prop = new Properties();
        configuration = new Configuration();
        try {
            for (Map.Entry<String, Configuration> map : yamlMap.entrySet()) {
                configuration = map.getValue();
                prop.setProperty(map.getKey() + "_browserName", configuration.getBrowserName());
                prop.setProperty(map.getKey() + "_platformName", configuration.getPlatformName());
                prop.setProperty(map.getKey() + "_browserVersion", configuration.getBrowserVersion());
                prop.setProperty(map.getKey() + "_sslCertificate", String.valueOf(configuration.getSslCertificate()));
                prop.setProperty(map.getKey() + "_headless", String.valueOf(configuration.getHeadless()));
                prop.setProperty(map.getKey() + "_URL", configuration.getAppURL());
                prop.setProperty(map.getKey() + "_OS", System.getProperty("os.name"));
                prop.setProperty(map.getKey() + "_OS_Version", System.getProperty("os.version"));
            }
            os = new FileOutputStream("C:\\Users\\gsonare\\gouravSonare\\Test\\Automation\\testAutomationFramework\\test-framework\\src\\main\\resources\\environment.properties");
            prop.store(os,"Browser Configuration Info");

        } catch (IOException ex) {
            logger.error(ex);
            BasicConfigurator.configure();
            ex.printStackTrace();
        }
    }
}