package gourav.framework.core.testInitialization;

import com.aventstack.extentreports.Status;
import gourav.framework.core.listener.Reporter;
import gourav.framework.core.listener.TestListener;
import gourav.framework.core.utility.CaptureScreenshot;
import gourav.framework.core.utility.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

@Listeners({TestListener.class})
public class BrowserInitializer {
	private static WebDriver driver;
	private static void setChromeDriverProperty(){
		System.setProperty("webdriver.chrome.driver", Constants.chrome);
	}
	private static void setFirefoxDriverProperty(){
		System.setProperty("webdriver.gecko.driver", Constants.gecko);
	}
	private static void setIEDriverProperty(){
		System.setProperty("webdriver.ie.driver", Constants.ie);
	}

	@BeforeTest
	private void setDriver(String browserName, String appURL, boolean headLess) {
		switch (browserName) {
		case "chrome":
			driver = initChromeDriver(appURL, headLess);
			break;
		case "firefox":
			driver = initFirefoxDriver(appURL, headLess);
			break;
		case "ie":
			driver = initIEDriver(appURL);
			break;
		default:
			System.out.println("browser : " + browserName + " is invalid, Launching Firefox as browser of choice..");
			driver = initFirefoxDriver(appURL, headLess);
		}
	}

	private static WebDriver initChromeDriver(String appURL, boolean headLess) {
		System.out.println("Launching google chrome with new profile..");
		setChromeDriverProperty();
		System.setProperty("headless", String.valueOf(headLess));
		System.out.println(headLess);
		if(headLess) {
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--headless");
			driver = new ChromeDriver(chromeOptions);
		} else {
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		return driver;
	}

	private static WebDriver initFirefoxDriver(String appURL, Boolean headLess) {
		System.out.println("Launching Firefox browser..");
		setFirefoxDriverProperty();
		System.setProperty("headless", String.valueOf(headLess));
		System.out.println(headLess);
		if(headLess) {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addArguments("--headless");
            driver = new FirefoxDriver(firefoxOptions);
        } else {
            driver = new FirefoxDriver();
        }
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to(appURL);
		return driver;
	}

	private static WebDriver initIEDriver(String appURL) {
		System.out.println("Launching internet explorer with new profile..");
		setIEDriverProperty();
		driver = new InternetExplorerDriver();
//		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		return driver;
	}

    @AfterTest
    public void tearDown() {
        if(driver != null) {
//			driver.close();
            driver.quit();
        }
    }

	@BeforeClass
	@Parameters({"browserName", "appURL", "headLess"})
	public void initializeTestBaseSetup(String browserType, String appURL, Boolean headLess) {
		try {
			System.out.println(browserType + ", " + appURL + ", " + headLess);
			setDriver(browserType, appURL, headLess);

		} catch (Exception e) {
			System.out.println("Error....." + e.getStackTrace());
            Reporter.setupReports(getClass().getName(), getClass().getSimpleName());
		}
	}

    @AfterClass
    public void afterClass()
    {
        System.out.println("In the AFTER CLASS");
        Reporter.flushReports();
    }

    @AfterMethod
    public void tearDown(ITestResult result)
    {
        System.out.println("In the AFTER METHOD " + result.getStatus() + result.getThrowable());
        if(result.getStatus()==ITestResult.FAILURE)
        {
            CaptureScreenshot.captureFailedScreenshot(driver, result.getName());
            Reporter.logResult(Status.ERROR, result.getName()+"Failed");
            Reporter.logThrowable(Status.ERROR, result.getThrowable());
        }
        else if(result.getStatus()==ITestResult.SKIP)
        {
            CaptureScreenshot.captureSkippedScreenshot(driver, result.getName());
            Reporter.logResult(Status.SKIP, result.getName()+ "Skipped");
            Reporter.logThrowable(Status.SKIP, result.getThrowable());
        }
        else if(result.getStatus()==ITestResult.SUCCESS)
        {
        	System.out.println("ABCD");
            CaptureScreenshot.capturePassedScreenshot(driver, result.getName());
            Reporter.logResult(Status.PASS, result.getName()+ "Passed");
        }
    }

	public WebDriver getDriver() {
		return driver;
	}
}