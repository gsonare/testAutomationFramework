package gourav.tests.homepage;

import gourav.framework.core.testInitialization.BrowserInitializer;
import gourav.pageobjects.homepage.GoogleHomePage;
import org.testng.annotations.Test;

public class GoogleHomePageTests extends BrowserInitializer {

    @Test
    public void homepageTests() {
        GoogleHomePage googleHomepage = new GoogleHomePage (getDriver());
        googleHomepage.Google();
    }
}