package gourav.regression.test;

import gourav.framework.core.browserConfiguration.Configuration;
import gourav.framework.core.utility.RetryListenerClass;
import gourav.framework.core.utility.Utils;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.annotations.BeforeSuite;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WebAppTest {

    public static Map<String, Configuration> yamlConfig;

    @BeforeSuite
    public void execute() {
        //read YAML file
        yamlConfig = Utils.readYAML();

        //Create TestNG
        createTestNG(yamlConfig);

        //Update Environment properties file
        Utils.updateEnvironmentFile();
    }

    public static void createTestNG(Map<String, Configuration> hmConfig) {
        //Create an instance of TestNG
        TestNG testNG = new TestNG();

        //Create an instance of XML Suite and assign a name for it
        XmlSuite suite = new XmlSuite();
        suite.setName("Dynamic Suite");
        suite.setConfigFailurePolicy(XmlSuite.FailurePolicy.CONTINUE);
        String className = "";
        String listenerName = "";
        for (Map.Entry<String, Configuration> map : hmConfig.entrySet()) {
            Configuration config = map.getValue();
            // Creating an instance of XmlTest and assigning a name for it
            XmlTest test = new XmlTest(suite);
            test.setName(map.getKey());

            // Set parameters to test
            test.addParameter("appURL", "http://"+config.getAppURL());
            test.addParameter("browserName", config.getBrowserName());
            test.addParameter("headLess", String.valueOf(config.getHeadless()));
            System.out.println(config.getPlatformName());
            System.out.println(config.getBrowserName());
            System.out.println(config.getHeadless());

			if (config.getPlatformName().equalsIgnoreCase("windows")) {
				className = "gourav.tests.homepage.GoogleHomePageTests";
				listenerName = "gourav.framework.core.utility.RetryListenerClass";
			}
			else if (config.getPlatformName().equalsIgnoreCase("linux")) {
                className = "gourav.tests.homepage.GoogleHomePageTests";
                listenerName = "gourav.framework.core.utility.RetryListenerClass";
            }

            // New list for the classes
            List<XmlClass> classes = new ArrayList<>();
            // Putting the classes to the list
            classes.add(new XmlClass(className));
            // Add classes to test
            test.setClasses(classes);

        }

        // New list for the Suites
        List<XmlSuite> suites = new ArrayList<>();

        // Add suite to the list
        suites.add(suite);

        // Creating the xml
        TestListenerAdapter tla = new TestListenerAdapter();
        List<Class> listenerClasses = new ArrayList<Class>();
        listenerClasses.add(RetryListenerClass.class);
        testNG.setUseDefaultListeners(true);
        testNG.addListener(tla);
//        testNG.setListenerClasses(listenerClasses);
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}